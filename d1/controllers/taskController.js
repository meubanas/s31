// contain all business logic

const Task = require('../models/taskSchema')

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Delete task
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removeTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {

			return removedTask
		}
	})
}

//  update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updateTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}

// activity s31

module.exports.getTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		} else {

			return result;
		}
	})
}



module.exports.completeTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = 'complete';
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}