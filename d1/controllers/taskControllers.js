// contain all business logic

const Task = require('../models/taskSchema')

// Get All Tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Creating New Task

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {

			return removedTask
		}
	})
}

// update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}


// Activity

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.getTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){

			console.log(error);
			return false;

		// Find successful, returns the task object back to the client/Postman
		} else {

			return result;	
		}
	})

}

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.completeTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){

			console.log(err);
			return false;

		}

		// Change the status of the returned document to "complete"
		result.status = "complete";

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
		return result.save().then((updatedTask, saveErr) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr) {

				console.log(saveErr);
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return false;

			// Update successful, returns the updated task object back to the client/Postman
			} else {

				// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
				return updatedTask;

			}
		})
	})

}
