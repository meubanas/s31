// Contain all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

// route for getting all tasks
router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// route for creating task

router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// route for deleting task

router.delete('/deleteTask/:id', (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


// Activity

// route for retrieving a single task

router.get("/:id", (req, res) => { 
		taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));	
})

// route for updating task's status

router.put("/:id/complete", (req, res) => {
		taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
}) 


module.exports = router